use std::{collections::HashMap, io::stdin};

use reqwest as req;

use tracing::{info, metadata::LevelFilter};
use tracing_subscriber::{fmt, prelude::*, EnvFilter};

const API_PATH: &str = "https://api-ipv4.porkbun.com/api/json/v3";

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
	tracing_subscriber::registry()
		.with(fmt::layer())
		.with(
			EnvFilter::builder().with_default_directive(LevelFilter::INFO.into()).from_env_lossy(),
		)
		.init();

	let mut secretkey = String::new();
	print!("Enter API secret: ");
	stdin().read_line(&mut secretkey).unwrap();
	// NOTE: read_line includes the newline, remove the whitespace
	secretkey = String::from(secretkey.trim());
	let apikey = "pk1_954a1036948940dedde3f07976b62f8645b33d8b3c2b1e1139e87c115465aab1";

	dbg!(&secretkey);
	dbg!(&apikey);

	info!("Getting IP address");
	let client = req::Client::new();
	let resp = client
		.post(format!("{API_PATH}/ping"))
		.body(format!(
			r#"{{"secretapikey":"{secretkey}","apikey":"{apikey}"}}"#
		))
		.send()
		.await?
		.json::<HashMap<String, String>>()
		.await?;

	info!("{resp:?}");
	assert_eq!(resp["status"], "SUCCESS");

	info!("Setting IP address of record");
	let ip = resp["yourIp"].clone();

	let resp = client
		.post(format!("{API_PATH}/dns/editByNameType/ky-bean.com/A/@"))
		.body(format!(
			r#"{{"secretapikey":"{secretkey}","apikey":"{apikey}","content":"{ip}"}}"#
		))
		.send()
		.await?
		.json::<HashMap<String, String>>()
		.await?;

	info!("{resp:?}");
	assert_eq!(resp["status"], "SUCCESS");

	Ok(())
}
