use std::path::{Path, PathBuf};

use directories::ProjectDirs;

pub fn project_dirs() -> ProjectDirs {
	ProjectDirs::from("", "", env!("CARGO_PKG_NAME")).unwrap()
}

pub fn config_file() -> PathBuf {
	let dirs = project_dirs();
	let mut config_dir = dirs.config_dir().to_path_buf();
	config_dir.push("Porkbun.toml");
	config_dir
}

pub fn secret_api_key_file() -> PathBuf {
	let dirs = project_dirs();
	let mut config_dir = dirs.config_dir().to_path_buf();
	config_dir.push("secret_api_key");
	config_dir
}
pub fn api_key_file() -> PathBuf {
	let dirs = project_dirs();
	let mut config_dir = dirs.config_dir().to_path_buf();
	config_dir.push("api_key");
	config_dir
}
