use serde::{Deserialize, Serialize};

use crate::map_invalid;
use crate::v0::wrapper::MapInvalid;

use super::json::OrNull;

#[derive(Debug, Clone, Copy, PartialOrd, PartialEq, Ord, Eq)]
#[repr(transparent)]
pub struct Id(u64);
impl Id {
	pub fn new(id: u64) -> Self {
		Self(id)
	}
	pub fn from_json_string(
		value: &serde_json::Value,
	) -> crate::v0::wrapper::Result<Self> {
		Ok(Self::new(
			value
				.as_str()
				.map_invalid(map_invalid!(|| {
					"\"id\" field is not a string"
				}))?
				.parse::<u64>()
				.map_invalid(map_invalid!(|| {
					"\"id\" string could not be parsed as an unsigned integer"
				}))?,
		))
	}
	pub fn as_u64(&self) -> u64 {
		self.0
	}
}
impl ToString for Id {
	fn to_string(&self) -> String {
		self.0.to_string()
	}
}
#[derive(
	Clone, Copy, Debug, Serialize, Deserialize, PartialOrd, PartialEq, Ord, Eq,
)]
#[repr(transparent)]
#[serde(transparent)] // TODO: ensure serde adheres to the restrictions on u64 emplaced by this type (i.e. ensure always >=600)
pub struct Ttl(u64);
impl Ttl {
	pub fn new(count: u64) -> crate::v0::wrapper::Result<Self> {
		if count >= 600 {
			Ok(Self(count))
		} else {
			Err(map_invalid!("\"ttl\" value must be at least 600"))?
		}
	}
	pub fn from_json_string(
		value: &serde_json::Value,
	) -> crate::v0::wrapper::Result<Self> {
		Self::new(
			value
				.as_str()
				.map_invalid(map_invalid!(|| {
					"\"ttl\" field is not a string"
				}))?
				.parse::<u64>()
				.map_invalid(map_invalid!(|| {
					"\"ttl\" string could not be parsed as an unsigned integer"
				}))?,
		)
	}
	pub fn as_u64(&self) -> u64 {
		self.0
	}
}
impl Default for Ttl {
	fn default() -> Self {
		Self(600)
	}
}
impl ToString for Ttl {
	fn to_string(&self) -> String {
		self.0.to_string()
	}
}

#[derive(
	Clone, Copy, Debug, Serialize, Deserialize, PartialOrd, PartialEq, Ord, Eq,
)]
#[repr(transparent)]
#[serde(transparent)] // no restrictions to consider on u64's accepted
pub struct Priority(u64);
impl Priority {
	pub fn new(prio: u64) -> Self {
		Self(prio)
	}
	pub fn from_option(prio: Option<u64>) -> Option<Self> {
		prio.map(Self::new)
	}
	pub fn from_json_string(
		value: &serde_json::Value,
	) -> crate::v0::wrapper::Result<Option<Self>> {
		let string =
			value.as_str_or_null().map_invalid(map_invalid!(|| {
				"\"prio\" field is not a string"
			}))?;
		if let Some(string) = string.as_str() {
			Ok(Some(Self::new(string.parse::<u64>().map_invalid(
				map_invalid!(|| {
					"\"prio\" string could not be parsed as an unsigned integer"
				}),
			)?)))
		} else {
			Ok(None)
		}
	}
	pub fn as_u64(&self) -> u64 {
		self.0
	}
}
impl ToString for Priority {
	fn to_string(&self) -> String {
		self.0.to_string()
	}
}
