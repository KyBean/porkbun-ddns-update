use crate::v0::api::{AsJson, Endpoint};

#[derive(Debug)]
pub struct Request<Data> {
	endpoint: Endpoint,
	data: Data,
}
impl<Data: AsJson> Request<Data> {
	pub fn new(endpoint: Endpoint, data: Data) -> Self {
		Self { endpoint, data }
	}

	pub fn endpoint(&self) -> &str {
		self.endpoint.as_str()
	}
	pub fn endpoint_owned(&self) -> String {
		self.endpoint.to_string()
	}
	pub fn json(&self) -> serde_json::Value {
		self.data.as_json()
	}
}
