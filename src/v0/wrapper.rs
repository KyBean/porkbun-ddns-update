pub type Result<T> = core::result::Result<T, Box<dyn std::error::Error>>;

#[macro_export]
macro_rules! report {
	($level:ident, $($tt:tt)*) => {
		tracing::$level!($($tt)*)
	};
}

pub trait ReportMessage {
	#[inline]
	fn report<F>(self, message: F) -> Self
	where
		F: FnOnce() -> (),
		Self: Sized,
	{
		message();
		self
	}
}
impl<T> ReportMessage for T {}

pub trait ReportInvalid {
	fn invalid<F>(self, invalid: F) -> Self
	where
		F: FnOnce() -> (),
		Self: Sized;
}
impl<T> ReportInvalid for core::option::Option<T> {
	fn invalid<F>(self, invalid: F) -> Self
	where
		F: FnOnce(),
		Self: Sized,
	{
		if self.is_none() {
			invalid();
		}
		self
	}
}
impl<T, E> ReportInvalid for core::result::Result<T, E> {
	fn invalid<F>(self, invalid: F) -> Self
	where
		F: FnOnce(),
		Self: Sized,
	{
		if self.is_err() {
			invalid();
		}
		self
	}
}

pub trait MapInvalid<MappedInvalid> {
	type Invalid;
	type Mapped;
	fn map_invalid<F>(self, invalid: F) -> Self::Mapped
	where
		F: FnOnce(Self::Invalid) -> MappedInvalid,
		Self: Sized;
}
impl<T, E> MapInvalid<E> for Option<T> {
	type Invalid = ();

	type Mapped = core::result::Result<T, E>;

	fn map_invalid<F>(self, invalid: F) -> Self::Mapped
	where
		F: FnOnce(Self::Invalid) -> E,
		Self: Sized,
	{
		match self {
			Some(inner) => Ok(inner),
			None => Err(invalid(())),
		}
	}
}
impl<T, E, E2> MapInvalid<E2> for core::result::Result<T, E> {
	type Invalid = E;

	type Mapped = core::result::Result<T, E2>;

	fn map_invalid<F>(self, invalid: F) -> Self::Mapped
	where
		F: FnOnce(Self::Invalid) -> E2,
		Self: Sized,
	{
		self.map_err(invalid)
	}
}

#[macro_export]
macro_rules! map_invalid {
	(|| $msg:literal) => {
		|_| map_invalid!($msg)
	};
	(|| { $msg:literal }) => {
		|_| map_invalid!($msg)
	};
	($msg:literal) => {{
		let msg = $msg;
		crate::report!(error, msg);
		msg
	}};
}
