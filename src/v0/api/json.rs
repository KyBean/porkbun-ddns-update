pub trait FromJson {
	fn from_json(value: &serde_json::Value) -> crate::v0::wrapper::Result<Self>
	where
		Self: Sized;
}
impl FromJson for () {
	fn from_json(_value: &serde_json::Value) -> crate::v0::wrapper::Result<Self>
	where
		Self: Sized,
	{
		Ok(())
	}
}

pub trait AsJson {
	fn as_json(&self) -> serde_json::Value;
	fn into_json(self) -> serde_json::Value
	where
		Self: Sized,
	{
		self.as_json()
	}
}

#[derive(Debug, Clone, Copy)]
pub enum ValOrNull<Val> {
	Val(Val),
	Null,
}
impl<Val> ValOrNull<Val> {
	pub fn as_val(&self) -> Option<&Val> {
		if let Self::Val(j) = self {
			Some(j)
		} else {
			None
		}
	}
	pub fn into_val(self) -> Option<Val> {
		if let Self::Val(j) = self {
			Some(j)
		} else {
			None
		}
	}
	pub fn val_or(self, default: Val) -> Val {
		if let Self::Val(j) = self {
			j
		} else {
			default
		}
	}
	pub fn val_or_else<F: FnOnce() -> Val>(self, default: F) -> Val {
		if let Self::Val(j) = self {
			j
		} else {
			default()
		}
	}
}

use paste::paste;
macro_rules! impl_nullable_val {
	(&$ty:ty) => {
		paste! {
			impl<'ty> ValOrNull<&'ty $ty> {
				pub fn [<as_ $ty>](&self) -> Option<&$ty> {
					self.as_val().map(|s| *s)
				}
				pub fn [<into_ $ty>](self) -> Option<&'ty $ty> {
					self.into_val()
				}
				pub fn [<$ty _or>](self, default: &'ty $ty) -> &$ty {
					self.val_or(default)
				}
				pub fn [<$ty _or_else>]<F: FnOnce() -> &'ty $ty>(self, default: F) -> &'ty $ty {
					self.val_or_else(default)
				}
			}
		}
	};
	($ty:ty) => {
		paste! {
			impl ValOrNull<$ty> {
				pub fn [<as_ $ty>](&self) -> Option<&$ty> {
					self.as_val()
				}
				pub fn [<into_ $ty>](self) -> Option<$ty> {
					self.into_val()
				}
				pub fn [<$ty _or>](self, default: $ty) -> $ty {
					self.val_or(default)
				}
				pub fn [<$ty _or_else>]<F: FnOnce() -> $ty>(self, default: F) -> $ty {
					self.val_or_else(default)
				}
			}
		}
	};
}
impl_nullable_val!(bool);
impl_nullable_val!(f64);
impl_nullable_val!(i64);
impl_nullable_val!(u64);
impl_nullable_val!(&str);
// impl ValOrNull<bool> {
// 	pub fn bool
// }
pub trait OrNull {
	fn as_bool_or_null(&self) -> Option<ValOrNull<bool>>;
	fn as_f64_or_null(&self) -> Option<ValOrNull<f64>>;
	fn as_i64_or_null(&self) -> Option<ValOrNull<i64>>;
	fn as_u64_or_null(&self) -> Option<ValOrNull<u64>>;
	fn as_str_or_null(&self) -> Option<ValOrNull<&str>>;
	fn as_array_or_null(&self) -> Option<ValOrNull<&Vec<serde_json::Value>>>;
	fn as_array_mut_or_null(
		&mut self,
	) -> Option<ValOrNull<&mut Vec<serde_json::Value>>>;
	fn as_object_or_null(
		&self,
	) -> Option<ValOrNull<&serde_json::Map<String, serde_json::Value>>>;
	fn as_object_mut_or_null(
		&mut self,
	) -> Option<ValOrNull<&mut serde_json::Map<String, serde_json::Value>>>;
}
impl OrNull for serde_json::Value {
	fn as_bool_or_null(&self) -> Option<ValOrNull<bool>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_bool().map(ValOrNull::Val)
		}
	}

	fn as_f64_or_null(&self) -> Option<ValOrNull<f64>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_f64().map(ValOrNull::Val)
		}
	}

	fn as_i64_or_null(&self) -> Option<ValOrNull<i64>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_i64().map(ValOrNull::Val)
		}
	}

	fn as_u64_or_null(&self) -> Option<ValOrNull<u64>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_u64().map(ValOrNull::Val)
		}
	}

	fn as_str_or_null(&self) -> Option<ValOrNull<&str>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_str().map(ValOrNull::Val)
		}
	}

	fn as_array_or_null(&self) -> Option<ValOrNull<&Vec<serde_json::Value>>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_array().map(ValOrNull::Val)
		}
	}

	fn as_array_mut_or_null(
		&mut self,
	) -> Option<ValOrNull<&mut Vec<serde_json::Value>>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_array_mut().map(ValOrNull::Val)
		}
	}

	fn as_object_or_null(
		&self,
	) -> Option<ValOrNull<&serde_json::Map<String, serde_json::Value>>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_object().map(ValOrNull::Val)
		}
	}

	fn as_object_mut_or_null(
		&mut self,
	) -> Option<ValOrNull<&mut serde_json::Map<String, serde_json::Value>>> {
		if self.is_null() {
			Some(ValOrNull::Null)
		} else {
			self.as_object_mut().map(ValOrNull::Val)
		}
	}
}
