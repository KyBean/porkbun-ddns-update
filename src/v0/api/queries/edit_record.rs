use serde_json::{json, Value};

use crate::v0::api::{
	ApiKey, AsJson, Endpoint, FromJson, Id, Keys, Priority, RecordType,
	Request, SecretApiKey, Status, Ttl,
};

#[derive(Debug)]
pub struct Payload {
	keys: Keys,
	subdomain_name: Option<String>,
	record_type: RecordType,
	content: String,
	ttl: Option<Ttl>,
	priority: Option<Priority>,
}
impl Payload {
	pub fn new(
		domain: &str,
		id: Id,
		keys: Keys,
		subdomain_name: Option<String>,
		record_type: RecordType,
		content: String,
		ttl: Option<Ttl>,
		priority: Option<Priority>,
	) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(
			Self::endpoint(domain, id)?,
			Self {
				keys,
				subdomain_name,
				record_type,
				content,
				ttl,
				priority,
			},
		))
	}
	fn endpoint(domain: &str, id: Id) -> crate::v0::wrapper::Result<Endpoint> {
		const QUERY: &str = "dns/edit/";
		Endpoint::with_domain_id(QUERY, domain, id)
	}
}
impl AsJson for Payload {
	fn as_json(&self) -> Value {
		let mut value = json!({});
		value["secretapikey"] = json!(self.keys.secret_api_key());
		value["apikey"] = json!(self.keys.api_key());
		if let Some(name) = &self.subdomain_name {
			value["name"] = json!(name);
		}
		value["type"] = self.record_type.as_json();
		value["content"] = json!(self.content);
		if let Some(ttl) = self.ttl {
			value["ttl"] = json!(ttl.to_string());
		}
		if let Some(prio) = self.priority {
			value["prio"] = json!(prio.to_string());
		}

		value
	}
}

#[derive(Debug)]
pub struct Response {
	status: Status,
}
impl Response {
	pub fn status(&self) -> &Status {
		&self.status
	}
}
impl FromJson for Response {
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			status: Status::from_json(value)?,
		})
	}
}
