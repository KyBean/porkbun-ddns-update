use serde_json::{json, Value};

use crate::v0::api::{
	ApiKey, AsJson, Endpoint, FromJson, Id, Keys, Request, SecretApiKey, Status,
};

#[derive(Debug)]
pub struct Payload {
	keys: Keys,
}
impl Payload {
	pub fn new(
		domain: &str,
		id: Id,
		keys: Keys,
	) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(Self::endpoint(domain, id)?, Self { keys }))
	}
	fn endpoint(domain: &str, id: Id) -> crate::v0::wrapper::Result<Endpoint> {
		const QUERY: &str = "dns/delete/";
		Endpoint::with_domain_id(QUERY, domain, id)
	}
}
impl AsJson for Payload {
	fn as_json(&self) -> Value {
		json!({
			"secretapikey": self.keys.secret_api_key(),
			"apikey": self.keys.api_key()
		})
	}
}

#[derive(Debug)]
pub struct Response {
	status: Status,
}
impl Response {
	pub fn status(&self) -> &Status {
		&self.status
	}
}
impl FromJson for Response {
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			status: Status::from_json(value)?,
		})
	}
}
