pub mod endpoint;
pub mod json;
pub mod keys {
	#[derive(Debug, Clone)]
	pub struct Keys {
		secret_api_key: SecretApiKey,
		api_key: ApiKey,
	}
	impl Keys {
		pub fn new(secret_api_key: SecretApiKey, api_key: ApiKey) -> Self {
			Self {
				secret_api_key,
				api_key,
			}
		}
		pub fn secret_api_key(&self) -> &str {
			&self.secret_api_key.key()
		}
		pub fn api_key(&self) -> &str {
			&self.api_key.key()
		}
	}

	#[derive(Debug, Clone)]
	#[repr(transparent)]
	pub struct SecretApiKey(String);
	impl SecretApiKey {
		pub fn new(key: String) -> Self {
			Self(key)
		}
		pub fn key(&self) -> &str {
			&self.0
		}
	}
	#[derive(Debug, Clone)]
	#[repr(transparent)]
	pub struct ApiKey(String);
	impl ApiKey {
		pub fn new(key: String) -> Self {
			Self(key)
		}
		pub fn key(&self) -> &str {
			&self.0
		}
	}
}
pub mod newtype;
pub mod porkbun;
pub mod queries;
pub mod record;
pub mod request;
pub mod status;

pub use endpoint::Endpoint;
pub use json::{AsJson, FromJson};
pub use keys::{ApiKey, Keys, SecretApiKey};
pub use newtype::{Id, Priority, Ttl};
pub use porkbun::Porkbun;
pub use queries::{
	CreateRecordPayload, CreateRecordResponse, DeleteRecordPayload,
	DeleteRecordResponse, EditRecordPayload, EditRecordResponse, PingPayload,
	PingResponse, RetrieveRecordsPayload, RetrieveRecordsResponse,
};
pub use record::{Record, RecordType};
pub use request::Request;
pub use status::Status;
