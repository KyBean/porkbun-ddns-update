use std::fmt::Debug;

use reqwest::blocking::{Client, RequestBuilder};
use tracing::instrument;

use crate::report;
use crate::v0::{
	api::{
		queries::{
			CreateRecordPayload, CreateRecordResponse, DeleteRecordPayload,
			DeleteRecordResponse, EditRecordPayload, EditRecordResponse,
			PingPayload, PingResponse, RetrieveRecordsPayload,
			RetrieveRecordsResponse,
		},
		AsJson, FromJson, Request,
	},
	wrapper::ReportMessage,
};

#[derive(Debug)]
pub struct Porkbun;
impl Porkbun {
	#[instrument]
	pub fn ping(
		client: &Client,
		request: Request<PingPayload>,
	) -> crate::v0::wrapper::Result<PingResponse> {
		const NAME: &str = "Ping";
		let request = Self::build_request(client, request, NAME);
		let response = Self::send_request(request, NAME)?;
		Ok(response)
	}

	#[instrument]
	pub fn create_record(
		client: &Client,
		request: Request<CreateRecordPayload>,
	) -> crate::v0::wrapper::Result<CreateRecordResponse> {
		const NAME: &str = "Create Record";
		let request = Self::build_request(client, request, NAME);
		let response = Self::send_request(request, NAME)?;
		Ok(response)
	}

	pub fn edit_record(
		client: &Client,
		request: Request<EditRecordPayload>,
	) -> crate::v0::wrapper::Result<EditRecordResponse> {
		const NAME: &str = "Edit Record";
		let request = Self::build_request(client, request, NAME);
		let response = Self::send_request(request, NAME)?;
		Ok(response)
	}

	pub fn delete_record(
		client: &Client,
		request: Request<DeleteRecordPayload>,
	) -> crate::v0::wrapper::Result<DeleteRecordResponse> {
		const NAME: &str = "Delete Record";
		let request = Self::build_request(client, request, NAME);
		let response = Self::send_request(request, NAME)?;
		Ok(response)
	}

	pub fn retrieve_records(
		client: &Client,
		request: Request<RetrieveRecordsPayload>,
	) -> crate::v0::wrapper::Result<RetrieveRecordsResponse> {
		const NAME: &str = "Retrieve Records";
		let request = Self::build_request(client, request, NAME);
		let response = Self::send_request(request, NAME)?;
		Ok(response)
	}

	fn build_request<Req: AsJson + Debug>(
		client: &Client,
		request: Request<Req>,
		name: &str,
	) -> RequestBuilder {
		client
			.post(request.endpoint())
			.json(&request.json())
			.report(|| {
				report!(trace, ?client, ?request, "Built {} API request", name)
			})
	}
	fn send_request<Res: FromJson + Debug>(
		request: RequestBuilder,
		name: &str,
	) -> crate::v0::wrapper::Result<Res> {
		Ok(Res::from_json(
			&request.send()?.json::<serde_json::Value>()?.report(|| {
				report!(
					trace,
					"Sent {} API request and received a response",
					name
				)
			}),
		)?)
	}
}
