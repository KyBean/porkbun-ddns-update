use tracing::instrument;

use crate::v0::{
	api::FromJson,
	wrapper::{MapInvalid, ReportMessage},
};
use crate::{map_invalid, report};

#[derive(Debug)]
pub enum Status<T: FromJson = ()> {
	Success(T),
	Error(String),
}
impl<T: FromJson> Status<T> {
	/// Returns `true` if the status is [`Success`].
	///
	/// [`Success`]: Status::Success
	#[must_use]
	pub fn is_success(&self) -> bool {
		matches!(self, Self::Success(..))
	}

	/// Returns `true` if the status is [`Error`].
	///
	/// [`Error`]: Status::Error
	#[must_use]
	pub fn is_error(&self) -> bool {
		matches!(self, Self::Error(..))
	}

	#[must_use]
	pub fn as_success(&self) -> Option<&T> {
		if let Self::Success(v) = self {
			Some(v)
		} else {
			None
		}
	}

	#[must_use]
	pub fn as_error(&self) -> Option<&String> {
		if let Self::Error(v) = self {
			Some(v)
		} else {
			None
		}
	}
}
impl<T: FromJson> FromJson for Status<T> {
	#[instrument]
	fn from_json(
		value: &serde_json::Value,
	) -> crate::v0::wrapper::Result<Self> {
		let status = value
			.get("status")
			.map_invalid(map_invalid!(|| {
				"Response is missing \"status\" field"
			}))?
			.as_str()
			.map_invalid(map_invalid!(|| {
				"\"status\" field is not a string"
			}))?;

		fn get_error_message(
			value: &serde_json::Value,
		) -> crate::v0::wrapper::Result<String> {
			let message =
				value.get("message").map_invalid(map_invalid!(|| {
					"Error response is missing \"message\" field"
				}))?;
			if message.is_null() {
				Ok(String::new().report(|| {
					report!(warn, "Server failed to provide an error message with error status")
				}))
			} else {
				Ok(String::from(message.as_str().map_invalid(map_invalid!(
					|| { "\"message\" field is not a string" }
				))?))
			}
		}

		match status {
			"SUCCESS" => Ok(Status::Success(T::from_json(value)?)),
			"ERROR" => Ok(Status::Error(get_error_message(value)?)),
			_ => Err(map_invalid!("Unexpected status string"))?,
		}
	}
}
