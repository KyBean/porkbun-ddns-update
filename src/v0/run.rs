// retrieve existing records
// filter for records that the config specifies to update
// take note of the IDs of the records
// check current IP address
// if different than IP in existing record, edit the record with new IP

use reqwest::{blocking::Client, Url};
use std::collections::BTreeMap;
use std::collections::BTreeSet;
use tracing::instrument;

use crate::v0::wrapper::{MapInvalid, ReportMessage};
use crate::{map_invalid, report};

#[instrument]
pub fn run(
	config: crate::v0::config::Config,
) -> Result<(), Box<dyn std::error::Error>> {
	// Create reqwest client
	let client = Client::new();
	report!(trace, ?client, "Created http client instance");

	// Get API keys
	let keys = get_keys()?;
	report!(trace, ?keys, "Loaded keys from file");

	// Save the IP to set for the domains, either from the config's static IP or from the current
	// public IP of this system
	let ip = if let Some(static_ip) = config.static_ip() {
		static_ip.to_string()
	} else {
		get_ip(&client, keys.clone())?
	};

	// Get DNS records from the config
	let config_records = config.porkbun().records();
	// Create iterator of all the domains from the config's list of records
	let domains_iter = config_records.iter().map(|record| record.domain());

	// Build a collection mapping the full hostname to its domain name from the records.
	// TODO: Move this into a separate type/module with utility functions for getting the root
	// domain and subdomain
	// FIXME: This will incorrectly parse IPv{4,6} addresses at `url.host_str`. We shouldn't accept
	// IP addresses, so we should check if it can be parsed as an IP first and fail if so.
	let mut root_domains: BTreeMap<&str, String> = BTreeMap::new();
	for domain in domains_iter {
		// Parse the domain as a URL
		let url = Url::parse(domain)?;
		// Get just the host portion of the URL
		let host = url.host_str().unwrap();

		// Find the last two segments of the host portion of the URL
		let mut second_last = None;
		let mut last = None;
		for sub in host.split('.') {
			second_last = last.take();
			last = Some(sub);
		}

		// Format the last two segments into the domain name
		let root_domain = format!("{}.{}", second_last.unwrap(), last.unwrap());
		root_domains.insert(domain, root_domain);
	}
	// Collect a set of only the root domains
	let roots_only: BTreeSet<_> = root_domains.values().collect();
	// Fetch the current DNS records Porkbun is aware of for the roots the config specifies should be checked
	let mut porkbun_records = BTreeMap::new();
	for root in roots_only {
		get_records(&client, &root, keys.clone())?
			.into_iter()
			.for_each(|record| {
				porkbun_records.insert(root, record);
			});
	}
	// Make immutable
	let porkbun_records = porkbun_records;

	// For each record in the config
	for record in config_records {
		let domain = record.domain();
		// FIXME: Figure out how to pick between existing TTL/prio and config TTL/prio
		let ttl = record.ttl();
		let prio = record.priority();

		// Check for a matching record in Porkbun's data
		let matching_record = porkbun_records
			.values()
			.find(|&r| *r.record_type() == crate::v0::api::RecordType::A);

		// If there is a matching record, compare IPs, update if necessary
		// If no matching record, create a new record
		// TODO: Should TTL/prio mismatch also be considered for updating the records?
		if let Some(record) = matching_record {
			if record.content() != ip {
				let response = crate::v0::api::Porkbun::edit_record(
					&client,
					// TODO: Ensure the parameters to this call are correct
					crate::v0::api::EditRecordPayload::new(
						domain,
						*record.id(),
						keys.clone(),
						None,
						*record.record_type(),
						ip.clone(),
						Some(*record.ttl()),
						record.priority(),
					)?,
				)?;
				todo!("Check response");
			} else {
				report!(info, ?record, "IP is up to date, skipping");
			}
		} else {
			// If there is no matching record, create a new record
			// TODO: Check if the IP is v4 or v6, use A or AAAA record accordingly
			let response = crate::v0::api::Porkbun::create_record(
				&client,
				crate::v0::api::CreateRecordPayload::new(
					domain,
					keys,
					todo!("Get subdomain from domain name"),
					crate::v0::api::RecordType::A,
					ip,
					ttl,
					prio,
				)?,
			)?;
			todo!("Check response");
		}
	}

	Ok(())
}

// TODO: use `secrecy` to have some amount of security with secret keys probably
fn get_keys() -> crate::v0::wrapper::Result<crate::v0::api::Keys> {
	let secret_api_key_file = crate::v0::dirs::secret_api_key_file();
	let api_key_file = crate::v0::dirs::api_key_file();

	let secret_api_key =
		std::fs::read_to_string(&secret_api_key_file).report(|| {
			report!(
				error,
				secret_api_key = ?secret_api_key_file,
				"Failed to read secret API key file"
			)
		})?;
	let api_key = std::fs::read_to_string(&api_key_file).report(
		|| report!(error, api_key = ?api_key_file, "Failed to read API key file"),
	)?;

	let secret_api_key = crate::v0::api::SecretApiKey::new(secret_api_key);
	let api_key = crate::v0::api::ApiKey::new(api_key);

	Ok(crate::v0::api::Keys::new(secret_api_key, api_key))
}

// TODO: Change types to core::net::IpAddr and friends
fn get_ip(
	client: &Client,
	keys: crate::v0::api::Keys,
) -> crate::v0::wrapper::Result<String> {
	let response =
		crate::v0::api::Porkbun::ping(client, crate::v0::api::PingPayload::new(keys)?)?;
	let status = response.status();
	match status {
		crate::v0::api::Status::Success(success) => {
			Ok(success.your_ip().to_string())
		}
		crate::v0::api::Status::Error(error) => {
			report!(error, error, "Failed to ping Porkbun API");
			Err(error.as_str())?
		}
	}
}

fn get_records(
	client: &Client,
	domain: &str,
	keys: crate::v0::api::Keys,
) -> crate::v0::wrapper::Result<Vec<crate::v0::api::Record>> {
	let response = crate::v0::api::Porkbun::retrieve_records(
		client,
		crate::v0::api::RetrieveRecordsPayload::new(domain, keys)?,
	)?;
	let status = response.status();
	match status {
		crate::v0::api::Status::Success(success) => Ok(success.records().to_vec()),
		crate::v0::api::Status::Error(error) => {
			report!(error, error, "Failed to retrieve existing records");
			Err(error.as_str())?
		}
	}
}
