use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Config {
	static_ip: Option<String>,
	porkbun: Porkbun,
}
impl Config {
	pub fn new(porkbun: Porkbun) -> Self {
		Self {
			static_ip: None,
			porkbun,
		}
	}
	pub fn static_ip(&self) -> Option<&str> {
		self.static_ip.as_deref()
	}
	pub fn porkbun(&self) -> &Porkbun {
		&self.porkbun
	}
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Porkbun {
	records: Vec<Record>,
}
impl Porkbun {
	pub fn new(records: Vec<Record>) -> Self {
		Self { records }
	}
	pub fn records(&self) -> &[Record] {
		&self.records
	}
}
impl FromIterator<Record> for Porkbun {
	fn from_iter<T: IntoIterator<Item = Record>>(iter: T) -> Self {
		Self {
			records: iter.into_iter().collect(),
		}
	}
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct Record {
	domain: String,
	ttl: Option<crate::v0::api::Ttl>,
	priority: Option<crate::v0::api::Priority>,
}
impl Record {
	pub fn new(domain: String) -> Self {
		Self {
			domain,
			ttl: None,
			priority: None,
		}
	}
	pub fn set_ttl(mut self, ttl: crate::v0::api::Ttl) -> Self {
		self.ttl = Some(ttl);
		self
	}
	pub fn set_prio(mut self, priority: crate::v0::api::Priority) -> Self {
		self.priority = Some(priority);
		self
	}
	pub fn domain(&self) -> &str {
		&self.domain
	}
	pub fn ttl(&self) -> Option<crate::v0::api::Ttl> {
		self.ttl
	}
	pub fn priority(&self) -> Option<crate::v0::api::Priority> {
		self.priority
	}
}

#[cfg(test)]
mod test {
	use test_log::test;

	use super::*;

	#[test]
	fn full_loop() {
		let config = Config::new(Porkbun::new(vec![Record::new(
			String::from("example.com"),
		)]));
		let toml = toml::to_string(&config).unwrap();
		assert_eq!(config, toml::from_str(&toml).unwrap());
	}
	#[test]
	fn deserialize() {
		let toml = toml::from_str::<Config>(
			"[porkbun]\nrecords = [{domain = \"example.com\"}]",
		)
		.unwrap();
		let config = Config::new(Porkbun::new(vec![Record::new(
			String::from("example.com"),
		)]));
		assert_eq!(toml, config);

		let toml = toml::from_str::<Config>(
			r#"
			[porkbun]
			records = [
				{ domain = "example.com" },
				{ domain = "uwu.example.com", ttl = 42069, priority = 42 },
			]
			"#,
		)
		.unwrap();
		let config = Config::new(Porkbun::new(vec![
			Record::new(String::from("example.com")),
			Record::new(String::from("uwu.example.com"))
				.set_ttl(crate::v0::api::Ttl::new(42069).unwrap())
				.set_prio(crate::v0::api::Priority::new(42)),
		]));
		assert_eq!(toml, config);
	}
}
