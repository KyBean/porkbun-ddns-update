use core::ops::Deref;

use reqwest::Url;

use crate::v0::api::Id;

#[derive(Debug)]
pub struct Endpoint(Url);
impl Endpoint {
	fn stub() -> Url {
		Url::parse("https://porkbun.com/api/json/v3/").unwrap()
	}
	pub fn new(query: &str) -> crate::v0::wrapper::Result<Self> {
		assert!(!query.is_empty());
		assert!(
			query.chars().last().unwrap() == '/',
			"Query strings must end in a slash"
		);
		Ok(Self(Self::stub().join(query)?))
	}
	pub fn with_domain(
		query: &str,
		domain: &str,
	) -> crate::v0::wrapper::Result<Self> {
		assert!(!query.is_empty());
		assert!(!domain.is_empty());
		assert!(
			query.chars().last().unwrap() == '/',
			"Query strings must end in a slash"
		);
		assert!(domain.chars().last().unwrap() != '/', "domain name strings must be the plain name, without trailing slash");
		Ok(Self(Self::stub().join(query)?.join(domain)?))
	}
	pub fn with_domain_id(
		query: &str,
		domain: &str,
		id: Id,
	) -> crate::v0::wrapper::Result<Self> {
		assert!(!query.is_empty());
		assert!(!domain.is_empty());
		assert!(
			query.chars().last().unwrap() == '/',
			"query strings must end in a slash"
		);
		assert!(domain.chars().last().unwrap() != '/', "domain name strings must be the plain name, without trailing slash");
		Ok(Self(
			Self::stub()
				.join(query)?
				.join(domain)?
				.join(&id.as_u64().to_string())?,
		))
	}
	pub fn as_url(&self) -> &Url {
		&self.0
	}
	pub fn as_str(&self) -> &str {
		self.as_url().as_str()
	}
}
impl Deref for Endpoint {
	type Target = Url;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}
