use serde::{Deserialize, Serialize};
use tracing::instrument;

use crate::map_invalid;
use crate::v0::{
	api::{AsJson, FromJson, Id, Priority, Ttl},
	wrapper::MapInvalid,
};

use super::json::OrNull;

#[derive(
	Debug, Copy, Clone, Deserialize, Serialize, PartialEq, PartialOrd, Ord, Eq,
)]
pub enum RecordType {
	A,
	#[serde(rename = "MX")]
	Mx,
	#[serde(rename = "CNAME")]
	Cname,
	#[serde(rename = "ALIAS")]
	Alias,
	#[serde(rename = "TXT")]
	Txt,
	#[serde(rename = "NS")]
	Ns,
	#[serde(rename = "AAAA")]
	Aaaa,
	#[serde(rename = "SRC")]
	Src,
	#[serde(rename = "TLSA")]
	Tlsa,
	#[serde(rename = "CAA")]
	Caa,
}
impl FromJson for RecordType {
	#[instrument]
	fn from_json(value: &serde_json::Value) -> crate::v0::wrapper::Result<Self> {
		let record_type = value.as_str().map_invalid(map_invalid!(|| {
			r#""type" field is not a string"#
		}))?;
		match record_type {
			"A" => Ok(Self::A),
			"MX" => Ok(Self::Mx),
			"CNAME" => Ok(Self::Cname),
			"ALIAS" => Ok(Self::Alias),
			"TXT" => Ok(Self::Txt),
			"NS" => Ok(Self::Ns),
			"AAAA" => Ok(Self::Aaaa),
			"SRC" => Ok(Self::Src),
			"TLSA" => Ok(Self::Tlsa),
			"CAA" => Ok(Self::Caa),
			_ => Err(map_invalid!(
				r#""type" string contained unrecognized record type"#
			))?,
		}
	}
}
impl AsJson for RecordType {
	fn as_json(&self) -> serde_json::Value {
		serde_json::Value::String(self.to_string())
	}
}
impl ToString for RecordType {
	fn to_string(&self) -> String {
		match self {
			RecordType::A => String::from("A"),
			RecordType::Mx => String::from("MX"),
			RecordType::Cname => String::from("CNAME"),
			RecordType::Alias => String::from("ALIAS"),
			RecordType::Txt => String::from("TXT"),
			RecordType::Ns => String::from("NS"),
			RecordType::Aaaa => String::from("AAAA"),
			RecordType::Src => String::from("SRC"),
			RecordType::Tlsa => String::from("TLSA"),
			RecordType::Caa => String::from("CAA"),
		}
	}
}

#[derive(Debug, Clone, PartialOrd, PartialEq, Ord, Eq)]
pub struct Record {
	id: Id,
	name: String,
	record_type: RecordType,
	content: String,
	ttl: Ttl,
	priority: Option<Priority>,
	notes: Option<String>,
}
impl FromJson for Record {
	#[instrument]
	fn from_json(value: &serde_json::Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			id: Id::from_json_string(value.get("id").map_invalid(
				map_invalid!(|| r#"Response is missing "id" field"#),
			)?)?,
			name: value
				.get("name")
				.map_invalid(map_invalid!(|| {
					r#"Response is missing "name" field"#
				}))?
				.as_str()
				.map_invalid(map_invalid!(|| {
					r#""name" field is not a string"#
				}))?
				.to_owned(),
			record_type: RecordType::from_json(
				value.get("type").map_invalid(map_invalid!(|| {
					r#"Response is missing "type" field"#
				}))?,
			)?,
			content: value
				.get("content")
				.map_invalid(map_invalid!(|| {
					r#"Response is missing "content" field"#
				}))?
				.as_str()
				.map_invalid(map_invalid!(|| {
					r#""content" field is not a string"#
				}))?
				.to_owned(),
			ttl: Ttl::from_json_string(value.get("ttl").map_invalid(
				map_invalid!(|| r#"Response is missing "ttl" field"#),
			)?)?,
			priority: Priority::from_json_string(
				value.get("prio").map_invalid(map_invalid!(
					|| r#"Response is missing a "prio" field"#
				))?,
			)?,
			notes: value
				.get("notes")
				.map_invalid(map_invalid!(
					|| r#"Response is missing a "notes" field"#
				))?
				.as_str_or_null()
				.map_invalid(map_invalid!(
					|| r#""notes" field is not a string"#
				))?
				.as_str()
				.map(String::from),
		})
	}
}
impl Record {
	#[instrument]
	pub fn records_from_array(
		value: &[serde_json::Value],
	) -> crate::v0::wrapper::Result<Vec<Self>> {
		let mut records = Vec::with_capacity(value.len());
		for record in value.iter().map(Record::from_json) {
			records.push(record?);
		}
		Ok(records)
	}

	pub fn id(&self) -> &Id {
		&self.id
	}
	pub fn name(&self) -> &str {
		self.name.as_ref()
	}
	pub fn record_type(&self) -> &RecordType {
		&self.record_type
	}
	pub fn content(&self) -> &str {
		self.content.as_ref()
	}
	pub fn ttl(&self) -> &Ttl {
		&self.ttl
	}
	pub fn priority(&self) -> Option<Priority> {
		self.priority
	}
	pub fn notes(&self) -> Option<&str> {
		self.notes.as_deref()
	}
}
