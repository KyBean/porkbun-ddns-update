use serde_json::{json, Value};
use tracing::{error, instrument};

use crate::map_invalid;
use crate::v0::{api::Keys, wrapper::MapInvalid};

use crate::v0::api::{
	ApiKey, AsJson, Endpoint, FromJson, Id, Priority, RecordType, Request,
	SecretApiKey, Status, Ttl,
};

#[derive(Debug)]
pub struct Payload {
	keys: Keys,
	subdomain_name: Option<String>,
	record_type: RecordType,
	content: String,
	ttl: Option<Ttl>,
	priority: Option<Priority>,
}
impl Payload {
	pub fn new(
		domain: &str,
		keys: Keys,
		subdomain_name: Option<String>,
		record_type: RecordType,
		content: String,
		ttl: Option<Ttl>,
		priority: Option<Priority>,
	) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(
			Self::endpoint(domain)?,
			Self {
				keys,
				subdomain_name,
				record_type,
				content,
				ttl,
				priority,
			},
		))
	}
	fn endpoint(domain: &str) -> crate::v0::wrapper::Result<Endpoint> {
		const QUERY: &str = "dns/create/";
		Endpoint::with_domain(QUERY, domain)
	}
}
impl AsJson for Payload {
	fn as_json(&self) -> Value {
		let mut value = json!({});
		value["secretapikey"] = json!(self.keys.secret_api_key());
		value["apikey"] = json!(self.keys.api_key());
		if let Some(name) = &self.subdomain_name {
			value["name"] = json!(name);
		}
		value["type"] = self.record_type.as_json();
		value["content"] = json!(self.content);
		if let Some(ttl) = self.ttl {
			value["ttl"] = json!(ttl.to_string());
		}
		if let Some(prio) = self.priority {
			value["prio"] = json!(prio.to_string());
		}

		value
	}
}

#[derive(Debug)]
pub struct Success {
	id: Id,
}
impl FromJson for Success {
	#[instrument]
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self {
			id: Id::from_json_string(value.get("id").ok_or_else(|| {
				let error = "Response is missing 'id' field";
				error!(?value, "{}", error);
				error
			})?)
			.map_invalid(map_invalid!(|| {
				"'id' field is not a string of a u64"
			}))?,
		})
	}
}

#[derive(Debug)]
pub struct Response {
	status: Status<Success>,
}
impl FromJson for Response {
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			status: Status::from_json(value)?,
		})
	}
}
impl Response {
	pub fn status(&self) -> &Status<Success> {
		&self.status
	}
}
