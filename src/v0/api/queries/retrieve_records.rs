use serde_json::{json, Value};
use tracing::instrument;

use crate::map_invalid;
use crate::v0::{
	api::{ApiKey, Keys, SecretApiKey},
	wrapper::MapInvalid,
};

use crate::v0::api::{AsJson, Endpoint, FromJson, Id, Record, Request, Status};

#[derive(Debug)]
pub struct Payload {
	keys: Keys,
}
impl Payload {
	pub fn new(
		domain: &str,
		keys: Keys,
	) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(Self::endpoint(domain)?, Self { keys }))
	}
	pub fn new_one(
		domain: &str,
		id: Id,
		keys: Keys,
	) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(
			Self::endpoint_with_id(domain, id)?,
			Self { keys },
		))
	}

	const QUERY: &str = "dns/retrieve/";
	fn endpoint(domain: &str) -> crate::v0::wrapper::Result<Endpoint> {
		Endpoint::with_domain(Self::QUERY, domain)
	}
	fn endpoint_with_id(
		domain: &str,
		id: Id,
	) -> crate::v0::wrapper::Result<Endpoint> {
		Endpoint::with_domain_id(Self::QUERY, domain, id)
	}
}
impl AsJson for Payload {
	fn as_json(&self) -> Value {
		json!({
			"secretapikey": self.keys.secret_api_key(),
			"apikey": self.keys.api_key()
		})
	}
}

#[derive(Debug)]
pub struct Success {
	records: Vec<Record>,
}
impl Success {
	pub fn records(&self) -> &[Record] {
		&self.records
	}
}
impl FromJson for Success {
	#[instrument]
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self>
	where
		Self: Sized,
	{
		Ok(Self {
			records: Record::records_from_array(
				value
					.get("records")
					.map_invalid(map_invalid!(|| {
						"Response is missing \"records\" field"
					}))?
					.as_array()
					.map_invalid(map_invalid!(|| {
						"\"records\" field is not an array"
					}))?,
			)?,
		})
	}
}
#[derive(Debug)]
pub struct Response {
	status: Status<Success>,
}
impl Response {
	pub fn status(&self) -> &Status<Success> {
		&self.status
	}
}
impl FromJson for Response {
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			status: Status::from_json(value)?,
		})
	}
}
