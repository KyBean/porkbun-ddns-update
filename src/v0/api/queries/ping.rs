use serde_json::{json, Value};
use tracing::instrument;

use crate::map_invalid;
use crate::v0::{
	api::{ApiKey, Keys, SecretApiKey},
	wrapper::MapInvalid,
};

use crate::v0::api::{AsJson, Endpoint, FromJson, Request, Status};

#[derive(Debug)]
pub struct Payload {
	keys: Keys,
}
impl Payload {
	pub fn new(keys: Keys) -> crate::v0::wrapper::Result<Request<Self>> {
		Ok(Request::new(Self::endpoint()?, Self { keys }))
	}
	fn endpoint() -> crate::v0::wrapper::Result<Endpoint> {
		const QUERY: &str = "ping/";
		Endpoint::new(QUERY)
	}
}
impl AsJson for Payload {
	fn as_json(&self) -> Value {
		json!({
			"secretapikey": self.keys.secret_api_key(),
			"apikey": self.keys.api_key()
		})
	}
}

#[derive(Debug)]
pub struct Success {
	your_ip: String,
}
impl Success {
	pub fn your_ip(&self) -> &str {
		&self.your_ip
	}
}
impl FromJson for Success {
	#[instrument]
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			your_ip: String::from(
				value
					.get("yourIp")
					.map_invalid(map_invalid!(|| {
						"Response is missing 'yourIp' field"
					}))?
					.as_str()
					.map_invalid(map_invalid!(|| {
						"'yourIp' field is not a string"
					}))?,
			),
		})
	}
}
#[derive(Debug)]
pub struct Response {
	status: Status<Success>,
}
impl Response {
	pub fn status(&self) -> &Status<Success> {
		&self.status
	}
}
impl FromJson for Response {
	fn from_json(value: &Value) -> crate::v0::wrapper::Result<Self> {
		Ok(Self {
			status: Status::from_json(value)?,
		})
	}
}
